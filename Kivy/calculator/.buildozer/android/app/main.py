"""
Copyright DDY 2016
"""
from kivy.properties import StringProperty
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.widget import Widget
from sympy.parsing.sympy_parser import parse_expr
from sympy import *
from decimal import Decimal


class CalculatorWidget(Widget):

    result = StringProperty()

    def __init__(self, **kwargs):
        super(CalculatorWidget, self).__init__(**kwargs)
        result = "Result"
        self.result = str(result)

    def lol(self):
        try:
            self.result = str(
                Decimal(str(N(parse_expr(self.input.text.replace("^", "**"))))).normalize())
        except Exception:
            self.result = "Error"
            return


class CalculatorApp(App):

    def build(self):
        calculator = CalculatorWidget()
        print(calculator)
        return calculator


if __name__ == '__main__':
    CalculatorApp().run()