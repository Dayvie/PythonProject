def generator():
    yield 5
    yield 5
    yield 20

if  __name__ == "__main__":
    gen = generator()
    print(gen.__next__())
    print(gen.__next__())
    print(gen.__next__())
    print(gen.__next__())

