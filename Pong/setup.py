import cx_Freeze, sys

base = None

if sys.platform == "win32":
    base = "Win32GUI"


executables = [cx_Freeze.Executable(script="pong.py", base=base)]

cx_Freeze.setup(
    name="Dayvie_Pong",
    options={"build_exe": {"packages": ["numpy", "pygame", "ctypes"], "includes": ["numpy", "pygame", "math", "random", "ctypes"],
                           "include_files": ["click.mp3"]}},
    version="1",
    description="Pong",
    executables=executables,
    )
#, "include_files":[]