from PyQt4 import QtCore


class TimeManager:
    """
    Class which handles time
    """

    def __init__(self):
        self.launcher = None
        self.counted_seconds = 0
        self.counted_minutes = 0
        self.counted_hours = 0
        self.time_counter_process = None

    def register_launcher(self, launcher):
        self.launcher = launcher

    def start_timer(self):
        if self.time_counter_process is None:
            self.time_counter_process = QtCore.QTimer()
            self.time_counter_process.setInterval(1000)
            self.launcher.gui_handler.connect(
                    self.time_counter_process, QtCore.SIGNAL('timeout()'), self.counter_process)
            self.time_counter_process.start()

    def counter_process(self):
        self.counted_seconds += 1
        if self.counted_seconds > 59:
            self.counted_seconds = 0
            self.counted_minutes += 1
        if self.counted_minutes > 59:
            self.counted_minutes = 0
            self.counted_hours += 1
        if self.launcher is not None:
            self.launcher.gui_handler.paint_counter(self.get_time_stamp())

    def pause_timer(self):
        if self.time_counter_process is not None:
            self.time_counter_process.stop()
        self.time_counter_process = None
        self.launcher.gui_handler.paint_counter(self.get_time_stamp())

    def stop_timer(self):
        if self.time_counter_process is not None:
            self.time_counter_process.stop()
        self.time_counter_process = None
        self.counted_seconds = 0
        self.counted_minutes = 0
        self.counted_hours = 0
        self.launcher.gui_handler.paint_counter(self.get_time_stamp())

    def get_time_stamp(self):
        seconds_string = str(self.counted_seconds)
        minutes_string = str(self.counted_minutes)
        hours_string = str(self.counted_hours)
        if self.counted_minutes < 10:
            minutes_string = "0" + minutes_string
        if self.counted_seconds < 10:
            seconds_string = "0" + seconds_string
        if self.counted_hours < 10:
            hours_string = "0" + hours_string
        return hours_string + ":" + minutes_string + ":" + seconds_string
