from clipboard import ClipBoard
import sys
from PyQt4 import QtGui


class ClipBoardLauncher:

    def __init__(self):
        self.clipboard = ClipBoard(self)

if __name__ == "__main__":
    sys.path.append("widgets")
    app = QtGui.QApplication(sys.argv)
    launcher = ClipBoardLauncher()
    sys.exit(app.exec_())
