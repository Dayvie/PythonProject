from PyQt4 import QtCore, QtGui, uic
import datetime


class TimerGui(QtGui.QMainWindow):

    def __init__(self, launcher):
        super().__init__()
        self.launcher = launcher
        self.qt_interface = uic.loadUi('main.ui')
        self.gui_refresher = QtCore.QTimer()
        self.gui_refresher.setInterval(1000/5)
        self.connect(self.gui_refresher, QtCore.SIGNAL('timeout()'), self.refresh_gui)
        self.connect(self.qt_interface.start, QtCore.SIGNAL('clicked()'),
                     self.launcher.time_manager.start)
        self.connect(self.qt_interface.pause, QtCore.SIGNAL('clicked()'),
                     self.launcher.time_manager.pause)
        self.connect(self.qt_interface.stop, QtCore.SIGNAL('clicked()'),
                     self.launcher.time_manager.stop)
        self.connect(self.qt_interface.note_edit, QtCore.SIGNAL("returnPressed()"),
                     self.handle_note_input)
        self.gui_refresher.start()
        self.paint_counter("00:00:00")
        self.qt_interface.show()

    def refresh_gui(self):
        now = datetime.datetime.now()
        self.qt_interface.date_label.setText(now.strftime("%Y-%m-%d %H:%M:%S"))

    def paint_counter(self, time_stamp):
        self.qt_interface.counter_lcd.display(time_stamp)

    def handle_note_input(self):
        self.qt_interface.note_label.setText(self.qt_interface.note_edit.text())
        self.qt_interface.note_edit.clear()

    def closeEvent(self, event):
        event.accept()
