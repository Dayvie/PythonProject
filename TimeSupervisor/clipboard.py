from PyQt4 import QtCore, QtGui, uic, Qt

class ClipBoard(QtGui.QWidget):

    def __init__(self, launcher):
        super().__init__()
        self.launcher = launcher
        self.qt_interface = uic.loadUi('clipboard.ui')
        self.connect(self.qt_interface.lineEdit, QtCore.SIGNAL("returnPressed()"),
                     self.handle_note_input)
        self.qt_interface.show()

    def handle_note_input(self):
        self.qt_interface.label.clear()
        self.qt_interface.label.appendPlainText(self.qt_interface.lineEdit.text())
