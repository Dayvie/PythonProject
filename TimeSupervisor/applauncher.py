from PyQt4 import QtCore, QtGui, uic
import sys, timerlauncher, clipboardlauncher


class AppLauncher(QtGui.QMainWindow):

    def __init__(self):
        super().__init__()
        self.qt_interface = uic.loadUi('applauncher.ui')
        self.apps = []
        self.connect(self.qt_interface.timer_button, QtCore.SIGNAL("clicked()"),
                     self.create_timer)
        self.connect(self.qt_interface.clipboard_button, QtCore.SIGNAL("clicked()"),
                     self.create_clipboard)
        self.qt_interface.show()

    def remove_app(self, removed_app):
        self.apps.remove(removed_app)
        print(self.apps)

    def create_clipboard(self):
        clipboardlauncher.ClipBoardLauncher()

    def create_timer(self):
        timerlauncher.TimerLauncher()

if __name__ == "__main__":
    sys.path.append("widgets")
    app = QtGui.QApplication(sys.argv)
    launcher = AppLauncher()
    sys.exit(app.exec_())
