import unittest
from PythonProject.TimeSupervisor.time_manager import TimeManager

class TestTimeManager(unittest.TestCase):

    def setUp(self):
        self.timer = TimeManager()

    def testTwoSeconds(self):
        self.timer.counter_process()
        self.timer.counter_process()
        self.assertEqual(2, self.timer.counted_seconds)

    def testOneMinute(self):
        for _ in range(0, 100):
            self.timer.counter_process()
        self.assertEqual(1, self.timer.counted_minutes)

    def testOneHour(self):
        for _ in range(0, 3700):
            self.timer.counter_process()
        self.assertEqual(1, self.timer.counted_hours)
