import sys
from PyQt4 import QtGui
from time_manager import TimeManager
from timergui import TimerGui


class TimerLauncher:

    def __init__(self):
        self.time_manager = TimeManager()
        self.time_manager.register_launcher(self)
        self.gui_handler = TimerGui(self)

if __name__ == "__main__":
    sys.path.append("widgets")
    app = QtGui.QApplication(sys.argv)
    launcher = TimerLauncher()
    sys.exit(app.exec_())
