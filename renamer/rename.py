import os, sys


def rename_all_local(prefix, identifier_string=""):
    """
    renames all files, without .py ending, in the directory in which this file lies in to a name with
        prefix + counter

        :parameter prefix: prefix of the filenames
        :parameter identifier: the identifier used to determine which files to rename
            the identifiert has to be in the filename for the file to get renamed

    """
    local_path = os.getcwd()
    file_names = os.listdir(local_path)
    file_name_counter = 1
    for file_name in file_names:
        file_extension = os.path.splitext(file_name)[1]
        if identifier_string in file_name and ".py" not in file_extension:
            os.rename(file_name, prefix +
                      "_" + str(file_name_counter) + file_extension)
            file_name_counter += 1


if __name__ == "__main__":
    arg_list = sys.argv
    if len(arg_list) == 2:
        rename_all_local(arg_list[1])
    if len(arg_list) == 3:
        rename_all_local(arg_list[1], identifier_string=arg_list[2])
