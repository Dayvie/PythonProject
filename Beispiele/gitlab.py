import requests

API_KEY = "zRNvuRope7n4AsrH7ie4"  # Enter your API key here
BASE_URL = "https://r-n-d.informatik.hs-augsburg.de:8080/api/v4"
headers = {'PRIVATE-TOKEN': API_KEY}

item_counter = 0
total_seconds = 0

for i in range(1, 57):  # manually set range of issues here. All issues doesn't work well.
    issue = requests.get(BASE_URL + '/projects/2954/issues/' + str(i) + '/time_stats', headers=headers)
    if 'total_time_spent' in issue.json():
        print(issue.json())
        total_seconds += issue.json()['total_time_spent']
    item_counter += 1

print("Hours on all issues: %.2f" % float((total_seconds / 60) / 60))
print("Total issues: " + str(item_counter))
