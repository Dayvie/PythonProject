from game_button_press import press_and_release
import game_button_press
import time


time.sleep(5)

FAST = 0.4


def use_deathcard_machine():
    """
    Slotmachine boring, 1% for ultra-rare, let the bot make the work!
    """
    while True:
        time.sleep(FAST)
        press_and_release(game_button_press.KEY_ENTER)
        time.sleep(FAST)
        press_and_release(game_button_press.KEY_ENTER)
        time.sleep(2.9)
        press_and_release(game_button_press.F1)
        time.sleep(FAST)
        press_and_release(game_button_press.KEY_ENTER)
        time.sleep(FAST)
        press_and_release(game_button_press.KEY_ENTER)


def unlock_gallery():
    while True:
        time.sleep(FAST)
        press_and_release(game_button_press.KEY_ENTER)
        time.sleep(FAST)
        press_and_release(game_button_press.KEY_ENTER)
        time.sleep(FAST)
        press_and_release(game_button_press.KEY_ARROW_DOWN)

def unlock_gallery2():
    while True:
        press_and_release(0x11)
        press_and_release(0x39)
        # a a a a a a atime.sleep(FAST)


if __name__ == "__main__":
    unlock_gallery2()
    # do_the_slot_machine()
