import numpy as np
from numpy.linalg import norm, inv
from numpy import cross, array, dot, transpose, inner

a = transpose(array([[2, 0, 0]]))
b = transpose(array([[0, 2, 0]]))
c = transpose(array([[0, 0, 2]]))

vector = np.cross(a-b, b-c, axis=0)

print(vector)
print(dot(vector, 1/norm(vector)))
print(norm(dot(vector, 1/norm(vector))))
print(float(1/3)*(a+b+c))


def normalizep(list):
    arr = None
    print(list)
    for victor in list:
        arr = array(victor)
        print(tuple(arr * 1/norm(arr)))

l = [(4,4,4),(4,4,4),(4,4,4)]
normalizep(l)

N = norm(array((4,4,4)))
L = norm(array((4,2,5)))

R = 2*(dot(L, N))*N - L
print(R)