import win32gui, time


def get_pixel_colour(i_x, i_y):
    i_desktop_window_id = win32gui.GetDesktopWindow()
    i_desktop_window_dc = win32gui.GetWindowDC(i_desktop_window_id)
    long_colour = win32gui.GetPixel(i_desktop_window_dc, i_x, i_y)
    i_colour = int(long_colour)
    return (i_colour & 0xff), ((i_colour >> 8) & 0xff), ((i_colour >> 16) & 0xff)


def loop():
    while True:
        print(get_pixel_colour(338, 888))
        print(get_pixel_colour(402, 888))
        print(get_pixel_colour(478, 888))
        print(get_pixel_colour(553, 888))
        time.sleep(0.1)


if __name__ == "__main__":
    loop()





