import pysolr


class SolrCore:
    def __init__(self, url: str):
        self.solr_core = pysolr.Solr(url, timeout=10)

    def index(self, data: list):
        self.solr_core.add(data)
        self.solr_core.optimize()

    def search(self, query: str) -> list:
        return self.solr_core.search(query)


if __name__ == "__main__":
    core = SolrCore('http://192.168.178.24:8983/solr/dayvietron')
    core.index([
        {
            "id": "doc_1",
            "title": "A test document"
        },
        {
            "id": "doc_2",
            "title": "The Banana: Tasty or Dangerous?"
        }
    ])
    core.index([{"id": "test",
                "title": "lul"}])
    results = core.search("*:*")
