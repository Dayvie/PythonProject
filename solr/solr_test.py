import pysolr

# Setup a Solr instance. The timeout is optional.
solr = pysolr.Solr('http://192.168.178.24:8983/solr/dayvietron', timeout=10)

# How you'd index data.
solr.add([
    {
        "id": "doc_1",
        "title": "A test document"
    },
    {
        "id": "doc_2",
        "title": "The Banana: Tasty or Dangerous?"
    }
])

# You can optimize the index when it gets fragmented, for better speed.
solr.optimize()

# Later, searching is easy. In the simple case, just a plain Lucene-style
# query is fine.
results = solr.search('bananas')

# The ``Results`` object stores total results found, by default the top
# ten most relevant results and any additional data like
# facets/highlighting/spelling/etc.
print("Saw {0} result(s).".format(len(results)))

# Just loop over it to access the results.
for result in results:
    print("The title is '{0}'.".format(result['title']))

results = solr.search('id:doc_*')

for result in results:
    print("The title is '{0}'.".format(result['title']))

