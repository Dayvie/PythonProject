from dataclasses import dataclass
import asyncio


@dataclass(frozen=True)
class MyPayload(object):
    count: int
    name: str


async def nested() -> MyPayload:
    await asyncio.sleep(1)
    return MyPayload(5, "S U C C")


async def main():
    task = asyncio.create_task(nested())
    # "task" can now be used to cancel "nested()", or
    # can simply be awaited to wait until it is complete:
    print(await nested())
    print(await task)

asyncio.run(main())
