import os, git, sys, psutil
import multiprocessing


def get_immediate_subdirectories(a_dir: str):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]


def pull_git(git_name: str) -> tuple:
    try:
        path = "./" + git_name
        g = git.cmd.Git(path)
        #g.stash()
        g.pull()
        return True, git_name
    except Exception:
        print("Error:", sys.exc_info()[0])
        return False, git_name


def pull_gits_in_local_folders() -> list:
    """
    gets all folder of the current directory and calls git stash and git pull
    on them in parallel
    """
    sub_directories = get_immediate_subdirectories(".")
    with multiprocessing.Pool(psutil.cpu_count()) as pool:
        return pool.map(pull_git, sub_directories)


if __name__ == '__main__':
    print(pull_gits_in_local_folders())
