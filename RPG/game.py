import pygame, ctypes, entities
from pytmx.util_pygame import load_pygame


user32 = ctypes.windll.user32
screen_size = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)
pygame.init()
game_display = pygame.display.set_mode(screen_size)
black = (0, 0, 0)
white = (255, 255, 255)
pygame.display.set_caption("A bit lol")


def leave(event=None):
    pygame.quit()
    quit()


def set_screen_prop():
    pygame.display.set_caption("Window")
    return pygame.display.set_mode(screen_size, pygame.FULLSCREEN)


def handle_input():
    key = pygame.key.get_pressed()
    if key[pygame.K_ESCAPE]:
        leave()


class Game:

    def __init__(self):
        self.window = set_screen_prop()
        self.entities = pygame.sprite.Group()
        self.display = game_display
        self.entity_factory = EntityFactory(self)
        self.player = self.entity_factory.produce_player()
        self.grills = [self.entity_factory.produce_grill()]

    def handle_controls(self, event):
        self.controls[event.key]()

    def release_controls(self, event):
        self.player.reset_movement()

    def game_loop(self):
        clock = pygame.time.Clock()
        while True:
            for event in pygame.event.get():
                handle_input()
            game_display.fill(black)
            self.entities.draw(self.window)
            self.entities.update()
            pygame.display.flip()
            clock.tick(60)


class EntityFactory:

    def __init__(self, game):
        self.display = game.display
        self.entities = game.entities
        self.game = game

    def produce_player(self):
        return entities.Player(screen_size[0] * 0.6, screen_size[1] * 0.4, self)

    def produce_grill(self, x=screen_size[0] * 0.4, y=screen_size[1] * 0.4):
        return entities.Grill(x, y, self)


class ScrolledGroup(pygame.sprite.Group):

    def draw(self, surface):
        for sprite in self.sprites():
            surface.blit(sprite.image, (sprite.rect.x - self.camera_x, sprite.rect.y))


class MapManager:

    def __init__(self):
        pass


if __name__ == "__main__":
    game = Game()
    game.game_loop()
