import pygame, game


class Entity(pygame.sprite.Sprite):

    def __init__(self, x, y, entity_factory):
        super(Entity, self).__init__(entity_factory.entities)
        self.entity_factory = entity_factory
        self.display = entity_factory.display
        self.x = x
        self.y = y
        self.image = None

    def update(self):
        pass

    def render(self):
        pass


class Grill(Entity):

    def __init__(self, x, y, entity_factory):
        super().__init__(x, y, entity_factory)
        self.image = pygame.image.load("res/img/sprite.png")
        self.rect = pygame.rect.Rect((x, y), self.image.get_size())

    def render(self):
        self.display.blit(self.image, (self.x, self.y))


class Player(Grill):

    def __init__(self, x, y, entity_factory):
        super().__init__(x, y, entity_factory)
        self.last = self.rect.copy()

    def move_up(self):
        self.rect.y -= 5

    def move_down(self):
        self.rect.y += 5

    def move_left(self):
        self.rect.x -= 5

    def move_right(self):
        self.rect.x += 5

    def update(self):
        last = self.rect.copy()
        key = pygame.key.get_pressed()
        if key[pygame.K_a]:
            self.move_left()
        if key[pygame.K_d]:
            self.move_right()
        if key[pygame.K_s]:
            self.move_down()
        if key[pygame.K_w]:
            self.move_up()
        for cell in pygame.sprite.spritecollide(self, self.entity_factory.game.grills, False):
            self.rect = last
