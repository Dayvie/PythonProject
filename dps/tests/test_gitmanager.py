import unittest
from PythonProject.dps.dps import GitManager


class GitManagerTests(unittest.TestCase):

    def setUp(self):
        self.dps = GitManager()

    def testSpace(self):
        for repo_name in self.dps.gits:
            if repo_name == " ":
                self.assertTrue(False)
        self.assertTrue(True)
