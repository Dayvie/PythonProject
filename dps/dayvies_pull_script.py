# -*- coding: utf-8 -*-
"""
David Yesil
David.Devran.Yesil@gmail.com
Copyright (c) 2016
"""

import git, inspect, pickle, sys, threading


class GitManager(object):
    """
    Class which handles git operations
    """
    def __init__(self):
        self.gits = None
        self.next_folder = "./"
        self.lock = threading.RLock()
        self.commands = \
            {"pull": self.pull_git, "printall": self.print_gits, "pullall": self.pull_gits,
             "add": self.add_repository, "del": self.del_repository,
             "q": self.exit, "help": self.print_help, "clone": self.clone_git,
             "cloneall": self.clone_all}
        self.load_gits()

    def clean_gits(self):
        if "" in self.gits:
            self.gits.remove("")
            print("removed empty repository")

    def clone_all(self):
        print("Cloning everything in mygits.txt")
        try:
            with open("mygits.txt", "r") as f:
                for line in f:
                    thread = threading.Thread(target=self.clone_git, args=(str(line).replace("\n",""),))
                    thread.start()
        except FileNotFoundError or EOFError:
            print("mygits.txt not found")

    def print_gits(self):
        for k in self.gits:
            print(k)

    def print_help(self):
        print("q: quits the program\n printall: prints all repositorynames"
              "\n pullall: pulls all gits which are saved internally in the program\n"
              " del <repoName>:deletes a repo\n add <repoName>: adds a repo"
              "\n clone <repoLink>: clones a repo and adds it to the "
              "\ncloneall: clones all repository-links in the mygits.txt file"
              "client\n pull <repoName>: pulls a certain repository")

    def clone_git(self, git_url):
        print("cloning: " + git_url)
        try:
            git.Git().clone(git_url)
            repo_name = git_url.split("/")
            repo_name = repo_name[len(repo_name)-1]
            repo_name = repo_name.split(".git")[0]
            with self.lock:
                self.add_repository(repo_name)
                self.save_gits()
        except git.exc.GitCommandError:
            print("cloning of " + git_url + " failed")
            return
        print(git_url+ " cloned")

    def exit(self):
        """
        closes the program
        :return:
        """
        print("shutting down")
        sys.exit(0)

    def pull_git(self, git_name):
        try:
            path = self.next_folder + git_name
            print("pulling.. ", path)
            g = git.cmd.Git(path)
            g.pull()
            print("Successfully pulled: " + git_name)
        except:
            print("Error:", sys.exc_info()[0])
            print("Failed pulling: " + git_name)
            return

    def pull_gits(self):
        """
        Pulls all Repositories in self.gits. Self.gits contains a String representation of the
        repositories
        """
        for folder_name in self.gits:
            thread = threading.Thread(target=self.pull_git, args=(folder_name,))
            thread.start()
        print("all threads fired off")

    def loop_shell(self):
        """
        starts the loop for the shell
        """
        while True:
            try:
                menue_eingabe = input(
                    "What's your demand? Enter <help> for a commandlist. \n")
                command = self.commands[menue_eingabe.split(" ")[0]]
                command_arg_length = len(inspect.getargspec(command).args)
                if command_arg_length == 1 and len(menue_eingabe.split(" ")) == 1:
                    command()
                elif command_arg_length == 2 and len(menue_eingabe.split(" ")) == 2:
                    command(menue_eingabe.split(" ")[1])
                elif command_arg_length == 3 and len(menue_eingabe.split(" ")) == 3:
                    command(menue_eingabe.split(" ")[1], menue_eingabe.split(" ")[2])
                else:
                    print("Command not found")
            except Exception:
                print("Command not found")

    def save_gits(self):
        """
        Saves the gits
        """
        with open("gits.pickle", "wb") as f:
            pickle.dump(self.gits, f)

    def load_gits(self):
        """
        Loads the paths for the repos
        """
        try:
            with open("gits.pickle", "rb") as f:
                self.gits = pickle.load(f)
        except FileNotFoundError or EOFError:
            print("Pickle not found -> Creating new one")
            self.gits = []
            self.save_gits()
        self.clean_gits()

    def add_repository(self, repo_name):
        """
        adds a string representation in self.gits
        """
        if repo_name in self.gits or repo_name == "./" or repo_name == " " or repo_name == "":
            print("repository " + repo_name + " not added to the internal gitlist")
            return
        else:
            self.gits.append(repo_name)
            self.save_gits()

    def del_repository(self, repo_name):
        """
        deletes a string representation in self.gits
        """
        try:
            self.gits.remove(repo_name)
            self.save_gits()
            print("deleted repository successfully")
        except ValueError:
            print("repository not found")

if __name__ == "__main__":
    git_handler = GitManager()
    arg_list = sys.argv
    print('Number of arguments:', len(arg_list), 'arguments.')
    print('Argument List:', str(arg_list))
    if len(arg_list) > 1:
        if arg_list[1] == "pullall":
            git_handler.pull_gits()
        elif arg_list[1] == "clone":
            git_handler.clone_git(arg_list[2])
        elif arg_list[1] == "pull":
            git_handler.pull_git(arg_list[2])
        elif arg_list[1] == "shell":
            print("Starting shell...")
            git_handler.loop_shell()
        elif arg_list[1] == "cloneall":
            git_handler.clone_all()
        else:
            print("Command not found, starting shell")
            git_handler.loop_shell()
    else:
        print("Starting shell...")
        git_handler.loop_shell()

__author__ = "david yesil"
