import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
includefiles = []
build_exe_options = {"packages": ["os"], "excludes": ["tkinter"], "include_files": includefiles}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
"""
if sys.platform == 'win32':
    base = 'Win32GUI'
"""
setup(name="Dayvie's_Pull Script",
        version = "0.1",
        description = "Dayvie's_Pull Script non_gui",
        options = {"build_exe": build_exe_options},
        executables = [Executable("dps.py", base=base)])
