from PyQt4 import QtCore, QtGui, uic
from dps import ObsEvent, GitManager
import sys


class GitGui(QtGui.QMainWindow):

    def __init__(self):
        super().__init__()
        self.event_mapping = {ObsEvent.git_list: self.refresh_gits, ObsEvent.console_post:
            self.post_console, ObsEvent.progess_bar: self.set_progress, ObsEvent.console_clear: self.clear_console}
        self.dps = GitManager(self)
        self.qt_interface = uic.loadUi('dps.ui')
        self.connect(self.qt_interface.clone_but, QtCore.SIGNAL('clicked()'),
                     self.dps.clone_all)
        self.connect(self.qt_interface.pull_but, QtCore.SIGNAL('clicked()'),
                     self.dps.pull_gits)
        self.connect(self.qt_interface.del_rep, QtCore.SIGNAL('clicked()'),
                     self.del_rep)
        self.connect(self.qt_interface.clone_rep, QtCore.SIGNAL('clicked()'),
                     self.clone)
        self.qt_interface.show()
        self.observe(ObsEvent.git_list)

    def clone(self):
        self.dps.clone_git(self.qt_interface.repo.text())
        self.qt_interface.repo.clear()

    def del_rep(self):
        self.dps.del_repository(self.qt_interface.git_list.currentItem().text())

    def observe(self, event, args=None):
        try:
            self.event_mapping[event](args)
        except KeyError:
            print(event + " is not an observer event")

    def refresh_gits(self, args):
        self.qt_interface.git_list.clear()
        for repo_name in self.dps.gits:
            self.qt_interface.git_list.addItem(repo_name)

    def set_progress(self, args):
        self.qt_interface.process.setValue(args[0])

    def post_console(self, args):
        self.qt_interface.console.addItem(args[0])

    def clear_console(self, args):
        self.qt_interface.console.clear()

if __name__ == "__main__":
    sys.path.append("widgets")
    app = QtGui.QApplication(sys.argv)
    launcher = GitGui()
    sys.exit(app.exec_())