'''
Created on 20.12.2015

@author: Christian
'''
import unittest
import numpy as np
from celestial_body_manager import CelestialBodyManager
from celestial_body_manager import CelestialBody


class tests(unittest.TestCase):

    def setUp(self):
        self.manager = CelestialBodyManager
        self.body1 = CelestialBody("planet1", np.array([10, 10, 10]), 10, 10, 10, 10)
        self.body2 = CelestialBody("planet2", np.array([10,10,10]), 10, 10, 10, 10)
        self.body3 = CelestialBody("planet2", np.array([23,1,16]), 10, 10, 10, 10)
        
        self.list_bodies = [self.body1, self.body2]

    def test_distance_constant(self):
        self.assertTrue(self.body2.distance_celestial_body(self.body3), self.body1.distance_celestial_body(self.body3))
        pass
    
    def test_distance_correct(self):
        self.assertEqual(0.0, self.body1.distance_celestial_body(self.body2))
        self.assertEqual(16.911534525287763, self.body1.distance_celestial_body(self.body3))
        
    def test_calculate_gravitational_acceleration_vector_constant(self):
        vector1 = self.body1.calculate_gravitational_acceleration_vector(self.body3)
        vector2 = self.body2.calculate_gravitational_acceleration_vector(self.body3)
        
        self.assertEqual(vector1[0], vector2[0])
        self.assertEqual(vector1[1], vector2[1])
        self.assertEqual(vector1[2], vector2[2])
    
    def test_calculate_gravitational_acceleration_vector_correct(self):
         vector1 = self.body1.calculate_gravitational_acceleration_vector(self.body3)
         
         self.assertEqual(1.7939020446827336e-12, vector1[0])
         self.assertEqual(-1.2419321847803539e-12 , vector1[1])
         self.assertEqual(8.2795478985356932e-13, vector1[2])
    
    def test_calculate_positional_vector_after_gravitational_acceleration_vector(self):
        pass
    
    def test_update_tangential_vector(self):
        test_vector = np.array([10, 10, 10])
        self.body1.update_tangential_vector(test_vector)
        
        self.assertEqual(110, self.body1.tangential_vector[0])
        self.assertEqual(110, self.body1.tangential_vector[1])
        self.assertEqual(110, self.body1.tangential_vector[2])
    
    
    def test_space_divisions(self):
        space = 10
        test_space = self.manager.make_space_division(self, space)

        Sector1 = ((-space, 0), (-space, 0), (space/3, 0))
        Sector2 = ((0, space), (0, space), (-space/3, 0))
        Sector3 = ((-space, 0), (-space, 0), (-space/3, 0))
        Sector4 = ((0, space), (0, space), (0, space/3))
        Sector5 = ((-space, 0), (0, space), (-space/3, 0))
        Sector6 = ((0, space), (-space, 0), (0, space/3))
        Sector7 = ((-space, 0), (0, space), (0, space/3))
        Sector8 = ((0, space), (-space, 0), (-space/3, 0))
        
        self.assertEqual(test_space.get('Quadrant1'), Sector1)
        self.assertEqual(test_space.get('Quadrant2'), Sector2)
        self.assertEqual(test_space.get('Quadrant3'), Sector3)
        self.assertEqual(test_space.get('Quadrant4'), Sector4)
        self.assertEqual(test_space.get('Quadrant5'), Sector5)
        self.assertEqual(test_space.get('Quadrant6'), Sector6)
        self.assertEqual(test_space.get('Quadrant7'), Sector7)
        self.assertEqual(test_space.get('Quadrant8'), Sector8)

    
    


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()