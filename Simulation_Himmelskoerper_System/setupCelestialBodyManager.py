# -*- coding: utf-8 -*-

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
try:
    from numpy.distutils.misc_util import get_numpy_include_dirs
    numpy_include_dirs = get_numpy_include_dirs()
except:
    numpy_include_dirs = []    
ext_modules=[Extension("celestial_body_manager", ["celestial_body_manager.pyx"],
             extra_compile_args=['-O3'],
             include_dirs = numpy_include_dirs)]
setup( name = 'Celestial_body_manager',
  cmdclass = {'build_ext': build_ext},
  ext_modules = ext_modules)