"""
import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
includefiles = ["galaxy.ui"]
build_exe_options = {"packages": ["os"], "excludes": ["tkinter"], "include_files": includefiles}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == 'win32':
    base = 'Win32GUI'

setup(name="Himmelskoerper_Simulation",
        version = "0.1",
        description = "Himmelskoerper_Simulation",
        options = {"build_exe": build_exe_options},
        executables = [Executable("logic_manager.py", base=base)])
"""
from distutils.core import setup
import py2exe, os
mfcfiles = [os.path.join(r"C:/Users/dayvie/Anaconda3/Lib/site-packages/pythonwin/", i)
            for i in ["mfc90.dll", "mfc90u.dll", "mfcm90.dll",
                      "mfcm90u.dll", "Microsoft.VC90.MFC.manifest"]]
data_files = [("Microsoft.VC90.MFC", mfcfiles)]
setup(data_files = data_files, console=['logic_manager.py'])
