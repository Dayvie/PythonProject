import pygame
import time
from multiprocessing import Process

def f():
    pygame.init()
    pygame.mixer.music.load("midi.mid")
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy():
        pygame.time.wait(1000)

if __name__ == '__main__':
    p = Process(target=f)
    p2 = Process(target=f)
    p2.start()
    time.sleep(1)
    p.start()
    p.join()
