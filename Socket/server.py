import socket, threading

def doSmth(conn, addr):
    print('Connected by', addr)
    data = conn.recv(1024)
    conn.sendall(data)

counter = 1
HOST = ''                 # Symbolic name meaning all available interfaces
PORT = 50007              # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
print("listening on port %s" % PORT)
while True:
    conn, addr = s.accept()
    threading.Thread(target=doSmth, args=(conn, addr)).start()
conn.close()

