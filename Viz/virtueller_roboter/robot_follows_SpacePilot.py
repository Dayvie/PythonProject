﻿# (c)2016 Peter.Roesch@hs-augsburg.de

from __future__ import print_function, division
import viztracker
import viz
import vizcam
import vizshape
import vizact
import math


import numpy as np
import RobotModel

def robot_follows_marker(robot, marker):
    robot_pos = robot.get_position()
    marker_pos = marker.getPosition()
    
    p = [   marker_pos[0] - robot_pos[0], 
            marker_pos[1] - robot_pos[1], 
            marker_pos[2] - robot_pos[2] ]
    
    ZF = np.array([
        [0, 1, 0, p[0]],
        [1, 0, 0, -p[2]],
        [0, 0, -1, p[1]],
        [0, 0, 0, 1]
        ], np.float32)
        
    robot.set_tool(ZF)


if __name__ == '__main__':

    #Enable full screen anti-aliasing (FSAA) to smooth edges
    viz.setMultiSample(4)

    # ---- Display ----
    viz.setOption ('viz.fullscreen', 1)
    viz.fov(40.0,1.333)
    # stereo
    #viz.setOption ('viz.stereo', viz.QUAD_BUFFER)
    viz.mouse.setVisible(False)

    # enable collision detection
    viz.MainView.collision(viz.ON) 
    
    #animation settings
    CYCLES_PER_SEC      = 0.05
    viz.setOption('viz.max_frame_rate', 60)
    viz.vsync(viz.ON)
    viz.go()
    
    ground = viz.addChild('ground_gray.osgb')
    robot = RobotModel.LBR_Model(r'STL')
    robot.set_position([0, 1.64, 5.5])
    
    connexion = viz.add('3dconnexion.dle')
    right_hand = connexion.addDevice()
    right_hand.setPosition(robot.get_position())
    right_hand.setTranslateScale([0.01, 0.01, 0.01])
    right_hand.setRotateScale([0, 0, 0])
    right_hand.setDominantAxisFilter(connexion.DOMINANT_ALL)
    
    ball = viz.addChild('ball.wrl', scale=[0.1, 0.1, 0.1])
    link2 = viz.link( right_hand, ball )
    
    vizact.ontimer(1./float(viz.getOption('viz.max_frame_rate')), 
        robot_follows_marker, robot, right_hand)
    
