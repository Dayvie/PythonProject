﻿# (c)2016 Peter.Roesch@hs-augsburg.de

from __future__ import print_function, division
import viz
import vizcam
import vizshape
import vizact
import math
import numpy as np
import RobotModel

def robot_follows_marker(robot, marker):
    robot_pos = robot.get_position()
    marker_pos = marker.getPosition()
    
    p = [   marker_pos[0] - robot_pos[0], 
            marker_pos[1] - robot_pos[1], 
            marker_pos[2] - robot_pos[2] ]
    
    ZF = np.array( [
        [0, 1, 0,  p[0]],
        [1, 0, 0, -p[2]],
        [0, 0, -1, p[1]],
        [0, 0, 0, 1]
        ], np.float32)
        
    robot.set_tool(ZF)
    
v = [0, 0, 0.25]
    
move_marker_forward = vizact.move(v, 2)
move_marker_backward = vizact.move([-v[0], -v[1], -v[2]], 2)
    

if __name__ == '__main__':
    CYCLES_PER_SEC      = 0.05
    viz.setMultiSample(4)
    viz.fov(60)
    viz.setOption('viz.max_frame_rate', 60)
    viz.vsync(viz.ON)
    viz.go()

    ground = viz.addChild('ground_gray.osgb')
    robot = RobotModel.LBR_Model(r'STL')
    robot.set_position([2.2, 1.64, 3])
    
    p = [2.5, 1.8, 3.2]
    
    ball = viz.addChild('ball.wrl', scale=[0.1, 0.1, 0.1])
    ball.setPosition(p[0], p[1], p[2])
    sequence = vizact.sequence(
        [move_marker_forward, move_marker_backward,
            move_marker_backward, move_marker_forward],
        viz.FOREVER)
    ball.addAction(sequence)
    
    vizact.ontimer(1./float(viz.getOption('viz.max_frame_rate')), 
        robot_follows_marker, robot, ball)