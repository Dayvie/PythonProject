﻿import viz
import vizcam
import vizshape
import vizact
import vizshape

viz.setMultiSample(4)
viz.fov(60)
viz.setOption('viz.max_frame_rate', 60)
viz.go()

#axes = vizshape.addAxes()
#axes.setPosition(0, 0, 0)

maze = viz.addChild('maze.osgb')
robo = viz.addChild("testrobo_all.stl")
#robo = viz.addChild("testrobo.stl")


scale = 0.001
robo.setScale(scale , scale , scale)
robo.setPosition(0, 0, 0)
