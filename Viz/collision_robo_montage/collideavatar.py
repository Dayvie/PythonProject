﻿'''
This example shows how to detect collisions
between an avatar bone and another model.
'''

import viz
import vizshape
import vizinfo
import vizact

viz.setMultiSample(4)
viz.fov(60)
viz.go()

#Enable physics
viz.phys.enable()

#Create ground plane
ground = viz.add('sky_day.osgb')
ground.collidePlane()

#Create static box in front of avatar
box = vizshape.addCube(size=2.0,pos=(0,1,1.7),color = viz.BLUE)
box.collideBox()
box.disable(viz.DYNAMICS)

#Create avatar that doesn't like big boxes
avatar = viz.add('vcc_female.cfg')
avatar.state(6)

#Create a collision box and link it to right hand of avatar
rHandBox = vizshape.addCube(size=0.1)
rHandBox.collideBox()
rHandBox.disable(viz.DYNAMICS)
rHandBox.enable(viz.COLLIDE_NOTIFY)
rHandLink = viz.link( avatar.getBone('Bip01 R Hand') , rHandBox )
#tweak the position of the box to cover the hand
rHandLink.preTrans([0.05,-0.05,0])
#Disable rendering on the box
rHandBox.disable(viz.RENDERING)

#Change color of the box to red when the avatar hits it
#and then back to blue after a short time
def onCollideBegin(e):
    if e.obj1 == rHandBox:
        if e.obj2 == box:
            box.color(viz.RED)
            vizact.ontimer2(.1,0,box.color,viz.BLUE)
viz.callback(viz.COLLIDE_BEGIN_EVENT,onCollideBegin)

panel = vizinfo.InfoPanel()
panel.addSeparator()
checkbox = panel.addLabelItem('Show Collide Box',viz.addCheckbox())

vizact.onbuttondown(checkbox,rHandBox.enable,viz.RENDERING)
vizact.onbuttonup(checkbox,rHandBox.disable,viz.RENDERING)

#Setup camera navigation
import vizcam
vizcam.PivotNavigate(center=[0,1,0],distance=4)
