﻿"""
This script demonstrates how to use collision detection.
Vizard will automatically handle collisions with the viewpoint.
When the viewpoint collides with an object, the script will print out "Collided"
"""
import viz

viz.setMultiSample(4)
viz.fov(60)
viz.go()

import vizinfo
vizinfo.InfoPanel()
#viz.gravity(1,6)

#Add the maze environment
maze = viz.addChild('maze.osgb')
import vizshape
vizshape.addAxes()
#Turn on collision
viz.collision(viz.ON)

#This function is called when a collision occurs
def mycollision(intersect):
	print intersect.point

#Create a callback for a collision event
viz.callback(viz.COLLISION_EVENT, mycollision)
