def generator():
    yield 6
    yield 7
    yield 8

if __name__ == "__main__":
    g = generator()
    for i in g:
        print(i)
    g = generator()
    print(g.__next__())