"""
@author David Yesil
License: BOYNEXTDOOR_1337
"""

import numpy as np
import time
print("wtf")

class TranscendentalCalculator:

    def __init__(self):
        self.odd_num = np.array([3])
        self.pi_divided_by_four = np.array([1 - 1/self.odd_num[0]])
        self.odd_num[0] += 2
        self.used_plus = False

    def calc_pi(self, n):
        for _ in range(n):
            self.calc_pi_step()
        return self.print_pi()

    def calc_pi_step(self):
        if self.used_plus:
            self.pi_divided_by_four[0] -= 1/self.odd_num[0]
            self.odd_num[0] += 2
            self.used_plus = False
        else:
            self.pi_divided_by_four[0] += 1/self.odd_num[0]
            self.odd_num[0] += 2
            self.used_plus = True

    def reset(self):
        self.odd_num[0] = 3
        self.pi_divided_by_four[0] = 1 - 1/self.odd_num[0]
        self.odd_num[0] = 5
        self.used_plus = False

    def print_pi(self):
        result = self.pi_divided_by_four[0]*4
        print(result)
        return result


calc = TranscendentalCalculator()
print(calc.calc_pi(10000000))
time.sleep(5)
