import unittest, matrix_trans
from numpy import transpose, array, dot


class MyTestCase(unittest.TestCase):
    def test_matrix_example(self):
        ak = transpose(array([[2, 0, 1]]))
        bk = transpose(array([[2, 1, 1]]))
        ck = transpose(array([[2, 0, 2]]))
        al = transpose(array([[-1, 0, 0]]))
        bl = transpose(array([[-1, 1, 0]]))
        cl = transpose(array([[-2, 0, 0]]))
        KLT = matrix_trans.create_coord_trans_matrix(ak, bk, ck, al, bl, cl)
        expected_transformation_matrix = array([[0., 0., 1., 2.],
                                                [0., 1., 0., 0.],
                                                [-1., 0., 0., 0.],
                                                [0., 0., 0., 1.]])
        self.assertEqual(str(expected_transformation_matrix), str(KLT))

""" TODO
    def ihhh_umlaute_test(self):
        ak = transpose(array([[2, 0, 1]]))
        bk = transpose(array([[2, 1, 1]]))
        ck = transpose(array([[2, 0, 2]]))
        al = transpose(array([[-1, 0, 0]]))
        bl = transpose(array([[-1, 1, 0]]))
        cl = transpose(array([[-2, 0, 0]]))
        KLT = matrix_trans.create_coord_trans_matrix(ak, bk, ck, al, bl, cl)
        test = dot(array([[2, 0, 1, 0]]), KLT)
        print(str(test))
"""


if __name__ == '__main__':
    unittest.main()
