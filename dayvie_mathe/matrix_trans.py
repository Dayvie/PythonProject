# -*- coding: utf-8 -*-
"""
Zeigt die Transformation von Koordinaten einer Basis in eine andere mit der Hilfe drei identischer Punkte in jedem
System. Basierend auf S.114 "Robotik mit Matlab" Georg Stark.
"""

import numpy as np
from numpy.linalg import norm, inv
from numpy import cross, array, dot, transpose
from threading import  Thread


__author__ = "David Yesil"


def create_transformation_matrix(ak, bk, ck, al, bl, cl):
    """
    Erschafft eine Transformationsmatrix welche Vektoren von einem Koordinatensystem l zu einem Koordinatensystem k
    übersetzt
    Args:
        ak: Ursprungspunkt in k
        bk: definiert die z-Achse in k mit der Hilfe von a
        ck: definiert die x-Achse in k mit a und b
        al: Ursprungspunkt in l
        bl: definiert die z-Achse in l mit der Hilfe von a
        cl: definiert die x-Achse in l mit a und b
    Returns: Eine Transformationsmatrix welche Vektoren von einer Basis l nach k mithilfe der Matrixmultiplikation
     übersetzen kann.
    """
    t1 = Thread(args=(ak, bk, ck), target=create_coord_matrix)
    t2 = Thread(args=(al, bl, cl), target=create_coord_matrix)
    KMT = t1.start()
    LMT = t2.start()
    t1.join()
    t2.join()
    return dot(KMT, inv(LMT))


def create_coord_matrix(a, b, c):
    """
    Args:
        a: Ursprungspunkt
        b: definiert mit a die z-Achse
        c: definiert mit dem Kreuzprodukt der z- und y-Achse die x-Achse
    Returns: eine homogene Matrix welche ein Koordinatensystem repräsentiert
    """
    point_of_origin = a
    z_axis = (b - a) / norm(b - a)
    y_axis = cross(z_axis, c - a, axis=0)
    y_axis = y_axis / norm(y_axis)
    x_axis = cross(y_axis, z_axis, axis=0)
    coordination_matrix = np.column_stack([x_axis, y_axis, z_axis, point_of_origin])
    e = array([[0, 0, 0, 1]])
    return np.concatenate((coordination_matrix, e), axis=0)


if __name__ == "__main__":
    ak = transpose(array([[2, 0, 1]]))
    bk = transpose(array([[2, 1, 1]]))
    ck = transpose(array([[2, 0, 2]]))

    al = transpose(array([[-1, 0, 0]]))
    bl = transpose(array([[-1, 1, 0]]))
    cl = transpose(array([[-2, 0, 0]]))

    KLT = create_transformation_matrix(ak, bk, ck, al, bl, cl)
    print("Transformationsmatrix: \n" + str(KLT))
