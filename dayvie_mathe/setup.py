import cx_Freeze

base = None

#if sys.platform == "win32":
#    base = "Console"


executables = [cx_Freeze.Executable(script="todes_pi.py")]

cx_Freeze.setup(
    name="mathelul",
    options={"build_exe": {"packages": ["numpy"], "includes": ["numpy"]
                           }},
    version="1",
    description="wtfmathe",
    executables=executables,
    entry_points={
          'console_scripts': [
              'my_project = todes_pi.__main__:main'
          ]}
    )
#, "include_files":[]