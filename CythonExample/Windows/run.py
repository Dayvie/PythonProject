import PythonProject.CythonExample.Windows.fib as fib
import cProfile
from typing import Callable


#what does our cython module contain?
#help(fib)


def multiple_calls(function: Callable, param: int, n_function_calls: int) -> None:
    for _ in range(n_function_calls):
        function(param)


def fib_python(n: int):
    """Print the Fibonacci series up to n. Slow python function"""
    a = 0
    b = 1
    while b < n:
        a, b = b, a + b


n_function_calls = 5000
n_fibonacci = 9999999999999999999


cProfile.run('multiple_calls(fib.fib, n_fibonacci, n_function_calls)')
cProfile.run('multiple_calls(fib_python, n_fibonacci, n_function_calls)')
