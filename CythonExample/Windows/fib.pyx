cpdef unsigned long long fib (unsigned long long n):
    """Print the Fibonacci series up to n. Superfast Cython cpdef"""
    cdef unsigned long long a = 0
    cdef unsigned long long b = 1
    while b < n:
        a, b = b, a + b
