class Unbekannt(object):
    def __init__(self):
        self.counter = 0
    def p(self, e):
        self.counter += 1
        if type(e) == str:
            print('#', self.counter, ':', e, '->', eval(e))
        else:
            print('#', self.counter, ':', e)
def qs(l, u):
    u.p(l)
    if len(l) <= 1:
        return l
    else:
        return qs([ x for x in l[1:] if x < l[0] ], u) \
        + [ l[0] ] + qs([ y for y in l[1:] if y >= l[0] ],u )

if __name__ == '__main__':
    u = Unbekannt()
    u.p('qs([88, -9, 22], u)')
    print(u.counter)
    u.p('u.counter')
