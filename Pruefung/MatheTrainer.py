import random

class Trainer:

    def __init__(self):
        self.exercises = {}
        self.operations = ('+', '-', '*')
        self.fails = 0

    def create_calc(self, id):
        num1 = random.randint(0, 9)
        num2 = random.randint(0, 9)
        operation = self.operations[random.randint(0, 2)]
        calc = (num1, num2, operation, False, id)
        if self.result(calc) < 0:
            calc = self.create_calc(id)
        return calc

    def create_calcs(self):
        for i in range(10):
            self.add_calc(i)

    def add_calc(self, id):
        calc = self.create_calc(id)
        while calc in tuple(self.exercises.values()):
            calc = self.create_calc(id)
        self.exercises[id] = calc

    def check_for_done(self):
        for i in tuple(self.exercises.values()):
            if i[3] is False:
                return False
        return True

    def result(self, tuple):
        if tuple[2] == '+':
            return tuple[0] + tuple[1]
        elif tuple[2] == '-':
            return tuple[0] - tuple[1]
        elif tuple[2] == '*':
            return tuple[0] * tuple[1]
        else:
            raise Exception("no valid operation")

    def train_start(self):
        self.create_calcs()
        while not self.check_for_done():
            self.ask()
        print("Fehler: " + str(self.fails))

    def ask(self):
        calc = random.choice(tuple(self.exercises.values()))
        while calc[3] is True:
            calc = random.choice(tuple(self.exercises.values()))
        print("%s %s %s = ?" % (calc[0], calc[2], calc[1]))
        if int(input()) == self.result(calc):
            self.exercises[calc[4]] = (calc[0], calc[1], calc[2], True, calc[4])
        else:
            self.fails += 1


if __name__ == "__main__":
    m = Trainer()
    m.train_start()