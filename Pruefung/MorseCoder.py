from __future__ import print_function, division

morseZeichenListe = [ '._', '_...', '_._.', '_..', '.', '.._.',
 '__.', '....', '..', '.___', '_._', '._..', '__', '_.', '___',
 '.__.', '__._', '._.', '...', '_', '.._', '..._', '.__', '_.._',
 '_.__', '__..' ]

buchstabenListe = [ chr(i) for i in range(ord('A'), ord('Z')+1)]

grussKlartext = 'Guten Morgen'

grussMorseZeichen = '__. .._ _ . _. / __ ___ ._. __. . _.'

def morseZeichenDekodieren(morseCode):
    s = ""
    wl = morseCode.split(" / ")
    for w in wl:
        bl = w.split(" ")
        for b in bl:
            print(b)
            index = morseZeichenListe.index(b)
            s += buchstabenListe[index]
        s += " "
    return s

def klartextKodieren(klarText):

    pass

if __name__ == '__main__':
    print(morseZeichenDekodieren(grussMorseZeichen))
    f = lambda x : x + 1
    print(f(2))