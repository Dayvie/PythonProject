import random

def exercise(l):
    a = random.choice(l)
    print(a)
    print(l[a:])
    print(l[:a])
    print(l[-a:])
    print(l[:-a])

if __name__ == "__main__":
    l = [x for x in range(20)]
    exercise(l)
    for i in l:
        print(str(i+i))