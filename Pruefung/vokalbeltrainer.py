import random

class VokabelTrainer:

    def __init__(self):
        self.list = {'cat': 'Katze', 'nose': 'Nase', 'flower': 'Blume'}
        self.richtig = 0
        self.falsch = 0

    def hinzufuegen(self, eng, de):
        self.list[eng] = de

    def zuruecksetzen(self):
        self.richtig = 0
        self.falsch = 0

    def ergebnisAusgabe(self):
        print('Richtig: %s Falsch: %s' % (self.richtig, self.falsch))

    def trainieren(self, n):
        for _ in range (n):
            vok = random.choice(tuple(self.list.keys()))
            eingabe = input('uebersetzen sie %s \n' % vok)
            if eingabe.lower() == self.list[vok].lower():
                print("richtig!")
                self.richtig += 1
            else:
                print("falsch!")
                self.falsch += 1

if __name__ == "__main__":
    vok = VokabelTrainer()
    vok.ergebnisAusgabe()
    vok.trainieren(1)