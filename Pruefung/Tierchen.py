class Haustier(object):
    def __init__(self, name, laut="nix"):
        self._name, self._laut = name, laut
    def gibLaut(self):
        print("%s sagt %s" % (self._name, self._laut))

class Hund(Haustier):
    def __init__(self, name):
        Haustier.__init__(self, name, "Wuff")

class Katze(Haustier):
    def __init__(self, name):
        Haustier.__init__(self, name)

def f(c, n="tierchen"):
    return c(n)

def func():
    list = [3, 4, 5, 6]
    a = list[1],
    a = a + 1
    return a

if __name__ == "__main__":
    a = 3 * "na"
    print(a[-4:] + "!")
    o = (f(Hund, "Snoopy"), f(Katze, "Garfield"), f(Haustier))
    for t in o:
        t.gibLaut()
