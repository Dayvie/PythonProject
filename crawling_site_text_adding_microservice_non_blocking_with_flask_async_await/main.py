from aiohttp import ClientSession
from functools import reduce
from flask import Flask
import asyncio


app = Flask(__name__)
loop = asyncio.get_event_loop()
urls = [
    'http://python-requests.org',
    'http://httpbin.org',
    'http://python-guide.org'
]


async def get_text_from_sites(urls):
    texts = []
    async with ClientSession() as session:
        get_requests = [*map(session.get, urls)]
        for get_request in get_requests:
            get_request = await get_request
            async with get_request:
                texts.append(await get_request.text())
    return texts


@app.route("/")
def hello():
    """
    :return: the added texts of the get-requests from the urls
    """
    texts = loop.run_until_complete(get_text_from_sites(urls))
    return reduce((lambda x, y: x + y), texts)


if __name__ == "__main__":
    app.run()
    loop.close()
